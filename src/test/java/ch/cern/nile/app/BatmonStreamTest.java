package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class BatmonStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/timber_data.json");
    private static final JsonObject DATA_FRAME_NO_MAXSNR_TIMESTAMP =
            TestUtils.loadRecordAsJson("data/timber_data_no_max_snr_timestamp.json");

    private static final JsonObject DATA_FRAME_NOT_TIMESTAMP =
            TestUtils.loadRecordAsJson("data/timber_data_no_timestamp.json");

    @Override
    public AbstractStream createStreamInstance() {
        return new BatmonStream();
    }

    @Test
    void givenDataFrame_whenDecoding_thenNoOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(232, outputRecord.value().get("packet_number").getAsInt());
        assertEquals(255, outputRecord.value().get("swbuild").getAsInt());
        assertEquals(255, outputRecord.value().get("dummy_byte").getAsInt());
        assertEquals((float) 3.30322265625, outputRecord.value().get("mon_3_3").getAsFloat());
        assertEquals((float) 5.006396484374999, outputRecord.value().get("mon_5").getAsFloat());
        assertEquals((float) 6.422335880002299, outputRecord.value().get("v_bat").getAsFloat());
        assertEquals(1177, outputRecord.value().get("extwtd_cnt").getAsInt());
        assertEquals(70824, outputRecord.value().get("fgdos_1_sensor_frequency").getAsInt());
        assertEquals(70964, outputRecord.value().get("fgdos_1_reference_frequency").getAsInt());
        assertEquals(0, outputRecord.value().get("fgdos_1_recharge_count").getAsInt());
        assertEquals(0, outputRecord.value().get("fgdos_1_recharge_frequency").getAsInt());
        assertEquals(73276, outputRecord.value().get("fgdos_2_sensor_frequency").getAsInt());
        assertEquals(75592, outputRecord.value().get("fgdos_2_reference_frequency").getAsInt());
        assertEquals(0, outputRecord.value().get("fgdos_2_recharge_count").getAsInt());
        assertEquals(0, outputRecord.value().get("fgdos_2_recharge_frequency").getAsInt());
        assertEquals(19, outputRecord.value().get("toshiba_m1_seu").getAsInt());
        assertEquals(13, outputRecord.value().get("toshiba_m2_seu").getAsInt());
        assertEquals(16, outputRecord.value().get("toshiba_m3_seu").getAsInt());
        assertEquals(15, outputRecord.value().get("toshiba_m4_seu").getAsInt());
        assertEquals(2, outputRecord.value().get("cypress_m5_seu").getAsInt());
        assertEquals(12, outputRecord.value().get("cypress_m6_seu").getAsInt());
        assertEquals(4, outputRecord.value().get("cypress_m7_seu").getAsInt());
        assertEquals(5, outputRecord.value().get("cypress_m8_seu").getAsInt());

        assertEquals("fcc23dfffe0f57a3", outputRecord.value().get("maxSnrRxInfo_gatewayID").getAsString());
        assertEquals("b5b8cfc4-c776-4a24-b27f-30e0a9c2bfa0",
                outputRecord.value().get("maxSnrRxInfo_uplinkID").getAsString());
        assertEquals("lora-0866-gw", outputRecord.value().get("maxSnrRxInfo_name").getAsString());
        assertEquals("2022-03-17T10:48:37.516201203Z", outputRecord.value().get("maxSnrRxInfo_time").getAsString());
        assertEquals(-70, outputRecord.value().get("maxSnrRxInfo_rssi").getAsInt());
        assertEquals(11.9, outputRecord.value().get("maxSnrRxInfo_loRaSNR").getAsDouble());
        assertEquals(46.25694, outputRecord.value().get("maxSnrRxInfo_location_latitude").getAsDouble());
        assertEquals(6.05851, outputRecord.value().get("maxSnrRxInfo_location_longitude").getAsDouble());
        assertEquals(470.0, outputRecord.value().get("maxSnrRxInfo_location_altitude").getAsDouble());

        assertEquals(867100000, outputRecord.value().get("txInfo_frequency").getAsLong());
        assertEquals(2, outputRecord.value().get("txInfo_dr").getAsInt());

        assertFalse(outputRecord.value().get("adr").getAsBoolean());
        assertEquals(687, outputRecord.value().get("fCnt").getAsInt());
        assertEquals(5, outputRecord.value().get("fPort").getAsInt());
        assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
    }


    @Test
    void givenDataFrameNoMaxSnrTimestamp_whenDecoding_thenOutputRecordIsCreatedWithExpectedTime() {
        pipeRecord(DATA_FRAME_NO_MAXSNR_TIMESTAMP);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(232, outputRecord.value().get("packet_number").getAsInt());
        assertEquals("2022-03-17T10:48:37.513000000Z", outputRecord.value().get("maxSnrRxInfo_time").getAsString());
        assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
    }

    @Test
    void givenDataFrameNotTimestamp_whenDecoding_thenOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME_NOT_TIMESTAMP);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertNotNull(outputRecord);
    }

}
