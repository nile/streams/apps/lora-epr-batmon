package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

public class BatmonPacketTest {

    private static final JsonElement DATA_FRAME =
            TestUtils.getDataAsJsonElement("6AD//wAAAggjDMUGmQSoFAE0FQEAAAAAPB4BSCcBAAAAABMADQAQAA8AAgAMAAQABQA=");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, BatmonPacket.class);
        assertEquals(232, packet.get("packet_number"));
        assertEquals(255, packet.get("swbuild"));
        assertEquals(255, packet.get("dummy_byte"));
        assertEquals(3.30322265625, packet.get("mon_3_3"));
        assertEquals(5.006396484374999, packet.get("mon_5"));
        assertEquals(6.422335880002299, packet.get("v_bat"));
        assertEquals(1177, packet.get("extwtd_cnt"));
        assertEquals(70824, packet.get("fgdos_1_sensor_frequency"));
        assertEquals(70964, packet.get("fgdos_1_reference_frequency"));
        assertEquals(0, packet.get("fgdos_1_recharge_count"));
        assertEquals(0, packet.get("fgdos_1_recharge_frequency"));
        assertEquals(73276, packet.get("fgdos_2_sensor_frequency"));
        assertEquals(75592, packet.get("fgdos_2_reference_frequency"));
        assertEquals(0, packet.get("fgdos_2_recharge_count"));
        assertEquals(0, packet.get("fgdos_2_recharge_frequency"));
        assertEquals(19, packet.get("toshiba_m1_seu"));
        assertEquals(13, packet.get("toshiba_m2_seu"));
        assertEquals(16, packet.get("toshiba_m3_seu"));
        assertEquals(15, packet.get("toshiba_m4_seu"));
        assertEquals(2, packet.get("cypress_m5_seu"));
        assertEquals(12, packet.get("cypress_m6_seu"));
        assertEquals(4, packet.get("cypress_m7_seu"));
        assertEquals(5, packet.get("cypress_m8_seu"));
    }
}
