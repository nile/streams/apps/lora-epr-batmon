// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.app.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;


/**
 * @see <a href="https://cernbox.cern.ch/index.php/s/sD0QRvGuEOWaxnQ">Source</a>
 */
public class BatmonPacket extends KaitaiStruct {
    public static BatmonPacket fromFile(String fileName) throws IOException {
        return new BatmonPacket(new ByteBufferKaitaiStream(fileName));
    }

    public BatmonPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public BatmonPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public BatmonPacket(KaitaiStream _io, KaitaiStruct _parent, BatmonPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.packetNumber = this._io.readU2le();
        this.swbuild = this._io.readU1();
        this.dummyByte = this._io.readU1();
        this.statusFlag = this._io.readU2le();
        this.mon33RawHidden = this._io.readU2le();
        this.mon5RawHidden = this._io.readU2le();
        this.vBatRawHidden = this._io.readU2le();
        this.extwtdCnt = this._io.readU2le();
        this.fgdos1SensorFrequency70Hidden = this._io.readU1();
        this.fgdos1SensorFrequency158Hidden = this._io.readU1();
        this.fgdos1SensorFrequency2316Hidden = this._io.readU1();
        this.fgdos1TargetFrequency70Hidden = this._io.readU1();
        this.fgdos1TargetFrequency158Hidden = this._io.readU1();
        this.fgdos1TargetFrequency2316Hidden = this._io.readU1();
        this.fgdos1RechargeCount = this._io.readU1();
        this.fgdos1RechargeFrequency70Hidden = this._io.readU1();
        this.fgdos1RechargeFrequency158Hidden = this._io.readU1();
        this.fgdos1RechargeFrequency2316Hidden = this._io.readU1();
        this.fgdos2SensorFrequency70Hidden = this._io.readU1();
        this.fgdos2SensorFrequency158Hidden = this._io.readU1();
        this.fgdos2SensorFrequency2316Hidden = this._io.readU1();
        this.fgdos2TargetFrequency70Hidden = this._io.readU1();
        this.fgdos2TargetFrequency158Hidden = this._io.readU1();
        this.fgdos2TargetFrequency2316Hidden = this._io.readU1();
        this.fgdos2RechargeCount = this._io.readU1();
        this.fgdos2RechargeFrequency70Hidden = this._io.readU1();
        this.fgdos2RechargeFrequency158Hidden = this._io.readU1();
        this.fgdos2RechargeFrequency2316Hidden = this._io.readU1();
        this.toshibaSeuM1 = this._io.readU2le();
        this.toshibaSeuM2 = this._io.readU2le();
        this.toshibaSeuM3 = this._io.readU2le();
        this.toshibaSeuM4 = this._io.readU2le();
        this.cypressSeuM5 = this._io.readU2le();
        this.cypressSeuM6 = this._io.readU2le();
        this.cypressSeuM7 = this._io.readU2le();
        this.cypressSeuM8 = this._io.readU2le();
    }
    private Integer fgdos2RechargeFrequency;

    /**
     * The recharge frequency of the FGDOS2 sensor.
     */
    public Integer fgdos2RechargeFrequency() {
        if (this.fgdos2RechargeFrequency != null)
            return this.fgdos2RechargeFrequency;
        int _tmp = (int) ((((fgdos2RechargeFrequency2316Hidden() * 65536) + (fgdos2RechargeFrequency158Hidden() * 256)) + fgdos2RechargeFrequency70Hidden()));
        this.fgdos2RechargeFrequency = _tmp;
        return this.fgdos2RechargeFrequency;
    }
    private Integer fgdos1RechargeFrequency;

    /**
     * The recharge frequency of the FGDOS1 sensor.
     */
    public Integer fgdos1RechargeFrequency() {
        if (this.fgdos1RechargeFrequency != null)
            return this.fgdos1RechargeFrequency;
        int _tmp = (int) ((((fgdos1RechargeFrequency2316Hidden() * 65536) + (fgdos1RechargeFrequency158Hidden() * 256)) + fgdos1RechargeFrequency70Hidden()));
        this.fgdos1RechargeFrequency = _tmp;
        return this.fgdos1RechargeFrequency;
    }
    private Double mon33;

    /**
     * 3.3V monitor in mV.
     */
    public Double mon33() {
        if (this.mon33 != null)
            return this.mon33;
        double _tmp = (double) (((mon33RawHidden() * 6.6) / 4096));
        this.mon33 = _tmp;
        return this.mon33;
    }
    private Double mon5;

    /**
     * 5V monitor in mV.
     */
    public Double mon5() {
        if (this.mon5 != null)
            return this.mon5;
        double _tmp = (double) (((mon5RawHidden() * 6.6) / 4096));
        this.mon5 = _tmp;
        return this.mon5;
    }
    private Integer fgdos1TargetFrequency;

    /**
     * The target frequency of the FGDOS1 sensor.
     */
    public Integer fgdos1TargetFrequency() {
        if (this.fgdos1TargetFrequency != null)
            return this.fgdos1TargetFrequency;
        int _tmp = (int) ((((fgdos1TargetFrequency2316Hidden() * 65536) + (fgdos1TargetFrequency158Hidden() * 256)) + fgdos1TargetFrequency70Hidden()));
        this.fgdos1TargetFrequency = _tmp;
        return this.fgdos1TargetFrequency;
    }
    private Integer fgdos1SensorFrequency;

    /**
     * The current frequency of the FGDOS1 sensor.
     */
    public Integer fgdos1SensorFrequency() {
        if (this.fgdos1SensorFrequency != null)
            return this.fgdos1SensorFrequency;
        int _tmp = (int) ((((fgdos1SensorFrequency2316Hidden() * 65536) + (fgdos1SensorFrequency158Hidden() * 256)) + fgdos1SensorFrequency70Hidden()));
        this.fgdos1SensorFrequency = _tmp;
        return this.fgdos1SensorFrequency;
    }
    private Double vBat;

    /**
     * Battery voltage.
     */
    public Double vBat() {
        if (this.vBat != null)
            return this.vBat;
        double _tmp = (double) ((((vBatRawHidden() * 3.3) / 0.2174) / 4096));
        this.vBat = _tmp;
        return this.vBat;
    }
    private Integer fgdos2TargetFrequency;

    /**
     * The target frequency of the FGDOS2 sensor.
     */
    public Integer fgdos2TargetFrequency() {
        if (this.fgdos2TargetFrequency != null)
            return this.fgdos2TargetFrequency;
        int _tmp = (int) ((((fgdos2TargetFrequency2316Hidden() * 65536) + (fgdos2TargetFrequency158Hidden() * 256)) + fgdos2TargetFrequency70Hidden()));
        this.fgdos2TargetFrequency = _tmp;
        return this.fgdos2TargetFrequency;
    }
    private Integer fgdos2SensorFrequency;

    /**
     * The current frequency of the FGDOS2 sensor.
     */
    public Integer fgdos2SensorFrequency() {
        if (this.fgdos2SensorFrequency != null)
            return this.fgdos2SensorFrequency;
        int _tmp = (int) ((((fgdos2SensorFrequency2316Hidden() * 65536) + (fgdos2SensorFrequency158Hidden() * 256)) + fgdos2SensorFrequency70Hidden()));
        this.fgdos2SensorFrequency = _tmp;
        return this.fgdos2SensorFrequency;
    }
    private int packetNumber;
    private int swbuild;
    private int dummyByte;
    private int statusFlag;
    private int mon33RawHidden;
    private int mon5RawHidden;
    private int vBatRawHidden;
    private int extwtdCnt;
    private int fgdos1SensorFrequency70Hidden;
    private int fgdos1SensorFrequency158Hidden;
    private int fgdos1SensorFrequency2316Hidden;
    private int fgdos1TargetFrequency70Hidden;
    private int fgdos1TargetFrequency158Hidden;
    private int fgdos1TargetFrequency2316Hidden;
    private int fgdos1RechargeCount;
    private int fgdos1RechargeFrequency70Hidden;
    private int fgdos1RechargeFrequency158Hidden;
    private int fgdos1RechargeFrequency2316Hidden;
    private int fgdos2SensorFrequency70Hidden;
    private int fgdos2SensorFrequency158Hidden;
    private int fgdos2SensorFrequency2316Hidden;
    private int fgdos2TargetFrequency70Hidden;
    private int fgdos2TargetFrequency158Hidden;
    private int fgdos2TargetFrequency2316Hidden;
    private int fgdos2RechargeCount;
    private int fgdos2RechargeFrequency70Hidden;
    private int fgdos2RechargeFrequency158Hidden;
    private int fgdos2RechargeFrequency2316Hidden;
    private int toshibaSeuM1;
    private int toshibaSeuM2;
    private int toshibaSeuM3;
    private int toshibaSeuM4;
    private int cypressSeuM5;
    private int cypressSeuM6;
    private int cypressSeuM7;
    private int cypressSeuM8;
    private BatmonPacket _root;
    private KaitaiStruct _parent;

    /**
     * Number of the packet.
     */
    public int packetNumber() { return packetNumber; }

    /**
     * Dummy Byte. Set to 0xFF (255).
     */
    public int swbuild() { return swbuild; }

    /**
     * Dummy Byte. Set to 0xFF (255).
     */
    public int dummyByte() { return dummyByte; }

    /**
     * The status flag. Always set to 0x0000 (0).
     */
    public int statusFlag() { return statusFlag; }

    /**
     * Raw reading by the ADC of the output of the Voltage Regulator which supplies 3.3V to the BatMon. The actual value can be calculated as mon_3_3_raw * 6.6 / 4096.
     */
    public int mon33RawHidden() { return mon33RawHidden; }

    /**
     * Raw reading by the ADC of the output of the Voltage Regulator which supplies 5V to the BatMon. The actual value can be calculated as mon_5_raw * 6.6 / 4096.
     */
    public int mon5RawHidden() { return mon5RawHidden; }

    /**
     * Raw reading by the ADC of the Voltage supplied by the Batteries. The actual value can be calculated as v_bat_raw * 3.3 / 0.2174 / 4096.
     */
    public int vBatRawHidden() { return vBatRawHidden; }

    /**
     * Incremental counter of the External Watchdog.
     */
    public int extwtdCnt() { return extwtdCnt; }

    /**
     * The current frequency of the FGDOS1 sensor (bits 23 down to 16).
     */
    public int fgdos1SensorFrequency70Hidden() { return fgdos1SensorFrequency70Hidden; }

    /**
     * The current frequency of the FGDOS1 sensor (bits 15 down to 8).
     */
    public int fgdos1SensorFrequency158Hidden() { return fgdos1SensorFrequency158Hidden; }

    /**
     * The current frequency of the FGDOS1 sensor (bits 7 down to 0).
     */
    public int fgdos1SensorFrequency2316Hidden() { return fgdos1SensorFrequency2316Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 23 down to 16).
     */
    public int fgdos1TargetFrequency70Hidden() { return fgdos1TargetFrequency70Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 15 down to 8).
     */
    public int fgdos1TargetFrequency158Hidden() { return fgdos1TargetFrequency158Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 7 down to 0).
     */
    public int fgdos1TargetFrequency2316Hidden() { return fgdos1TargetFrequency2316Hidden; }

    /**
     * Number of recharge done by FGDOS1.
     */
    public int fgdos1RechargeCount() { return fgdos1RechargeCount; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 23 down to 16).
     */
    public int fgdos1RechargeFrequency70Hidden() { return fgdos1RechargeFrequency70Hidden; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 15 down to 8).
     */
    public int fgdos1RechargeFrequency158Hidden() { return fgdos1RechargeFrequency158Hidden; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 7 down to 0).
     */
    public int fgdos1RechargeFrequency2316Hidden() { return fgdos1RechargeFrequency2316Hidden; }

    /**
     * The current frequency of the FGDOS2 sensor (bits 23 down to 16).
     */
    public int fgdos2SensorFrequency70Hidden() { return fgdos2SensorFrequency70Hidden; }

    /**
     * The current frequency of the FGDOS2 sensor (bits 15 down to 8).
     */
    public int fgdos2SensorFrequency158Hidden() { return fgdos2SensorFrequency158Hidden; }

    /**
     * The current frequency of the FGDOS2 sensor (bits 7 down to 0).
     */
    public int fgdos2SensorFrequency2316Hidden() { return fgdos2SensorFrequency2316Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 23 down to 16).
     */
    public int fgdos2TargetFrequency70Hidden() { return fgdos2TargetFrequency70Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 15 down to 8).
     */
    public int fgdos2TargetFrequency158Hidden() { return fgdos2TargetFrequency158Hidden; }

    /**
     * The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 7 down to 0).
     */
    public int fgdos2TargetFrequency2316Hidden() { return fgdos2TargetFrequency2316Hidden; }

    /**
     * Number of recharge done by FGDOS2
     */
    public int fgdos2RechargeCount() { return fgdos2RechargeCount; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 23 down to 16).
     */
    public int fgdos2RechargeFrequency70Hidden() { return fgdos2RechargeFrequency70Hidden; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 15 down to 8).
     */
    public int fgdos2RechargeFrequency158Hidden() { return fgdos2RechargeFrequency158Hidden; }

    /**
     * The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 7 down to 0).
     */
    public int fgdos2RechargeFrequency2316Hidden() { return fgdos2RechargeFrequency2316Hidden; }

    /**
     * The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M1).
     */
    public int toshibaSeuM1() { return toshibaSeuM1; }

    /**
     * The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M2).
     */
    public int toshibaSeuM2() { return toshibaSeuM2; }

    /**
     * The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M3).
     */
    public int toshibaSeuM3() { return toshibaSeuM3; }

    /**
     * The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M4).
     */
    public int toshibaSeuM4() { return toshibaSeuM4; }

    /**
     * The total number of SEUs measured by the Cypress Memory during the measurement period (M5).
     */
    public int cypressSeuM5() { return cypressSeuM5; }

    /**
     * The total number of SEUs measured by the Cypress Memory during the measurement period (M6).
     */
    public int cypressSeuM6() { return cypressSeuM6; }

    /**
     * The total number of SEUs measured by the Cypress Memory during the measurement period (M7).
     */
    public int cypressSeuM7() { return cypressSeuM7; }

    /**
     * The total number of SEUs measured by the Cypress Memory during the measurement period (M8).
     */
    public int cypressSeuM8() { return cypressSeuM8; }
    public BatmonPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
